﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models;

namespace UserManagement.Service
{
    interface IUserInformationRepository
    {
        Task<List<UserInformation>> GetAllUserInformations();
        Task<UserInformation> UpdateUserById(UserInformation user);
        Task<UserInformation> DeleteUserById(int id);
        Task<UserInformation> GetUserById(int id);
    }
}
