﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models;
using UserManagement.Service;

namespace UserManagement.Service
{
    public class UserInformationRepository : IUserInformationRepository
    {
        //get all data from text
        public async Task<List<UserInformation>> GetAllUserInformations()
        {
            List<UserInformation> userInformation = new List<UserInformation>();
            var logPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
            var getAllUser = Task.Run(() =>
            {
                var logFile = File.ReadAllLines(logPath);
                var logList = new List<string>(logFile);
                string a = string.Empty;
                logList.ForEach(f =>
                {
                    var arr = f.Split('|');
                    userInformation.Add(new UserInformation
                    {
                        Id = int.Parse(arr[0]),
                        FirstName = arr[1],
                        LastName = arr[2],
                        BillingAddress = arr[3],
                        DeliveryAddress = arr[4]
                    });
                });
            });
            await getAllUser;
            return userInformation;
        }
        //update
        public async Task<UserInformation> UpdateUserById(UserInformation user)
        {
            List<UserInformation> userInformation = new List<UserInformation>();
            var logPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
            var logFile = File.ReadAllLines(logPath);
            var logList = new List<string>(logFile);
            var getAllUser = Task.Run(() =>
            {
                //read all lines
                
                //find which line
                //overwrite the identified line
                //start in 1 because 0 is header
                for (int i = 1; i < logList.Count(); i++)
                {
                    var arr = logList[i].Split('|');
                    if (int.Parse(arr[0]) == user.Id && int.Parse(arr[0]) != 0)
                    {
                        logList[i] = user.Id + "|" + user.FirstName + "|" + user.LastName + "|" + user.BillingAddress + "|" + user.DeliveryAddress;
                    }
                    else
                    {
                        //unable to find
                    }
                }


                //rewrite file
                File.WriteAllText(logPath, String.Empty);
                File.WriteAllLines(logPath, logList);

            });
            await getAllUser;
            return user;
        }
        //delete
        public async Task<UserInformation> DeleteUserById(int id)
        {
            List<UserInformation> userInformation = new List<UserInformation>();
            var logPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
            
            var getAllUser = Task.Run(() =>
            {
                var logFile = File.ReadAllLines(logPath);
                var logList = new List<string>(logFile);
                var lineRemove = string.Empty;
                logList.ForEach(f =>
                {   
                    if (int.Parse(f.Split('|')[0]) == id)
                    {
                        lineRemove = f;
                    }
                });

                var linesToKeep = logFile.Where(w => w != lineRemove);
                File.WriteAllText(logPath, String.Empty);
                File.WriteAllLines(logPath, linesToKeep);
                return lineRemove;
            });
            var lineToRemove = await getAllUser;
            var arr = lineToRemove.Split('|');
            return new UserInformation
            {
                Id = int.Parse(arr[0]),
                FirstName = arr[1],
                LastName = arr[2],
                BillingAddress = arr[3],
                DeliveryAddress = arr[4]
            };
        }
        //get a user using if
        public async Task<UserInformation> GetUserById(int id)
        {
            UserInformation userInformation = new UserInformation();
            var logPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
            var getAllUser = Task.Run(() =>
            {
                var logFile = File.ReadAllLines(logPath);
                var logList = new List<string>(logFile);
                logList.ForEach(f =>
                {
                    var arr = f.Split('|');
                    if(int.Parse(arr[0]) == id)
                    {
                        userInformation = new UserInformation
                        {
                            Id = int.Parse(arr[0]),
                            FirstName = arr[1],
                            LastName = arr[2],
                            BillingAddress = arr[3],
                            DeliveryAddress = arr[4]
                        };
                    }
                    
                });
            });
            await getAllUser;
            return userInformation;
        }
        //save
        public async Task<UserInformation> SaveUser(UserInformation user)
        {
            List<UserInformation> userInformation = new List<UserInformation>();
            var logPath = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
            var logFile = File.ReadAllLines(logPath);
            var logList = new List<string>(logFile);
            var getAllUser = Task.Run(() =>
            {
                //get last id
                var nextId = int.Parse(logList[logList.Count() - 1].Split('|')[0]) + 1;
                //increment by 1
                var newUser = nextId + "|" + user.FirstName + "|" + user.LastName + "|" + user.BillingAddress + "|" + user.DeliveryAddress;
                File.AppendAllText(logPath, newUser + Environment.NewLine);

                var logPathx = AppDomain.CurrentDomain.BaseDirectory.ToString() + "UserInformations.txt";
                var logFilex = File.ReadAllLines(logPath);
                var logListx = new List<string>(logFile);
                return newUser;
            });
            await getAllUser;
            return user;
        }
    }
}
