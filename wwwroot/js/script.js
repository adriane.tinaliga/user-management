﻿var app = angular.module('userInformationApp', ['angularUtils.directives.dirPagination'])
app.controller('userInformationController', function ($scope, $http, $location, $window ) {


    $scope.currentPage = 1
    $scope.numPerPage = 10
    $scope.maxSize = 5;
    //get user list
    $scope.user = {};
    $scope.userList = [{}];
    $scope.editUserId = 0;
    getAllUser();
    getUser();
    //getUser

    function getAllUser() {
        $http({
            url: "/UserInformation/GetAllUsers",
            method: "GET"

        }).then(function (response) {
            console.log(response);
            
            if (response.status == 200) {
                $scope.userList = response.data;
                $scope.userList.shift();
            }
            else {
                //show modal
            }
        });
    }
    function getUser() {
        var id = localStorage.getItem('EditId');
        if (id != 0 && id != null) {
            $http({
                url: "/UserInformation/GetUserInformation",
                method: "GET",
                params: { userId: localStorage.getItem('EditId') },
                headers: {
                    "Content-Type": 'application/json',
                }
            }).then(function (response) {
                console.log(response);

                if (response.status == 200) {
                    $scope.user = response.data;
                }
                else {
                    //show modal
                }
            });
        }
        
    }
    //save user
    $scope.saveUser = function (user) {
        var data = {
            id: 0,
            firstName: user.firstName,
            lastName: user.lastName,
            BillingAddress: user.billingAddress,
            DeliveryAddress: user.billingAddress,
        }
        $http({
            url: "/UserInformation/SaveUserInformation",
            method: "POST",
            params: data,
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                localStorage.removeItem('EditId');
                alert("Successfully updated.");
                $window.location.href = '/UserInformation/';
                //go to index page

            }
            else {
                alert("There was a problem updating the user information.");
            }
        });
    }
    //update user
    $scope.updateUser = function (user) {
        var data = {
            id: localStorage.getItem('EditId'),
            firstName: user.firstName,
            lastName: user.lastName,
            BillingAddress: user.billingAddress,
            DeliveryAddress: user.billingAddress,
        }
        $http({
            url: "/UserInformation/UpdateUserInformation",
            method: "PUT",
            params: data,
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                localStorage.removeItem('EditId');
                alert("Successfully updated.");
                $window.location.href = '/UserInformation/';
                //go to index page
                
            }
            else {
                alert("There was a problem updating the user information.");
            }
        });
    }

    $scope.gotoEdit = function (id) {
        localStorage.setItem('EditId', id);
    }
    //delete user
    $scope.deleteUser = function (userId) {
        debugger;
        $http({
            url: "/UserInformation/DeleteUserInformation",
            method: "DELETE",
            params: { userId: userId },
            headers: {
                "Content-Type": 'application/json',
            }
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                getAllUser();
                alert("Successfully deleted.");
            }
            else {
                alert("There was a problem deleting the user information.");
            }
        });
    }


    $scope.sort = function (keyName) {
        $scope.sortKey = keyName;
        $scope.reverse = !$scope.reverse;
    }

    $scope.numPages = function () {
        return Math.ceil($scope.userList.length / $scope.numPerPage);
    };
    $scope.$watch('currentPage + numPerPage', function () {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
            , end = begin + $scope.numPerPage;

        $scope.filteredTodos = $scope.userList.slice(begin, end);
    });
});