﻿
Developed in Visual Studio 2019: ASP. Net MVC
Front-end: Angular.js 1.2.12
Compatibility browser: Google Chrome


How to use:
1. If you do not have Visual Studio 2019, install it first. You may download it here:
https://visualstudio.microsoft.com/downloads/
2. After installation, double click "UserManagement.sln". This will open the project.
3. After opening the project, you will find it in the upper-middler
part of Visual Studio IDE is the run button(IIS Express). Click the down icon and set
Google Chrome as default browser.
4. After setting the default browser, click the run button "IIS Express." A Google Chrome
will be automatically opened.
5. You may start testing the User Management Web Application.

