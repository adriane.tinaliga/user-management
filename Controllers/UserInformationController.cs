﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Service;
using UserManagement.Models;

namespace UserManagement.Controllers
{
    public class UserInformationController : Controller
    {
        UserInformationRepository _repository = new UserInformationRepository();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Edit()
        {
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }

        //get all user
        public async Task<List<UserInformation>> GetAllUsers()
        {
            var list = new List<UserInformation>();
            try
            {
                list = await _repository.GetAllUserInformations();
            }
            catch (Exception)
            {
                this.HttpContext.Response.StatusCode = 500;
                
            }
            return list;
        }
        
        //update user
        public async Task<UserInformation> UpdateUserInformation(UserInformation userInformation)
        {
            UserInformation user = new UserInformation();
            try
            {
                user = await _repository.UpdateUserById(userInformation);
            }
            catch (Exception)
            {
                this.HttpContext.Response.StatusCode = 500;
            }
            return user;

        }
        //delete
        
        public async Task<UserInformation> DeleteUserInformation(int UserId)
        {
            UserInformation user = new UserInformation();
            try
            {
                user = await _repository.DeleteUserById(UserId);
            }
            catch (Exception)
            {
                this.HttpContext.Response.StatusCode = 500;
            }
            return user;
        }
        //get user by id
        public async Task<UserInformation> GetUserInformation(int UserId)
        {
            UserInformation user = new UserInformation();
            try
            {
                user = await _repository.GetUserById(UserId);
            }
            catch (Exception)
            {
                this.HttpContext.Response.StatusCode = 500;
            }
            return user;
        }

        public async Task<UserInformation> SaveUserInformation(UserInformation userInformation)
        {
            UserInformation user = new UserInformation();
            try
            {
                user = await _repository.SaveUser(userInformation);
            }
            catch (Exception)
            {
                this.HttpContext.Response.StatusCode = 500;
            }
            return user;
        }
    }
}
